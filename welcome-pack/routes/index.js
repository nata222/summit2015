module.exports = function (app, addon) {

    // Root route. This route will serve the `atlassian-connect.json` unless the
    // documentation url inside `atlassian-connect.json` is set
    app.get('/', function (req, res) {
        res.format({
            // If the request content-type is text-html, it will decide which to serve up
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            // This logic is here to make sure that the `atlassian-connect.json` is always
            // served up when requested by the host
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });
    // button route to serve the iframe containing the create welcome page button
    app.get('/welcome-pack/button', addon.authenticate(), function(req, res){

        res.render('macro-button', {});
    });

    // dialog route to serve the dialog containing the form for creating the
    // welcome pack
    app.get('/welcome-pack/dialog', addon.authenticate(), function(req, res){

	   res.render('macro-dialog', {});
    });

    // POST welcome-pack route, this route creates the welcome pack on the 
    // confluence instance.
    app.post('/welcome-pack', function (req, res) {
            // call back to confluence to create content
            var token = addon.checkValidToken();
            var confluenceReq = createWelcomePackHome(req.body);
            res.json({result: 'success'});
            //createFirstWeekPlan(req.body);
            //createNinetyDayPlan(req.body);
            //createOngoingLearning(req.body);
            // if (!content){
            //     res.statusCode = 400;
            //     res.send('Add on server <i>error</i>');
            // } else {
            //     res.send('Created <a href='+content._links.webui+'>welcome pack</a>');                
            // }    


    });

    function createWelcomePackHome(data){
            var template = hbs.compile(fs.readFileSync('./views/welcome-pack-home.hbs','utf8'));
            var storageXHTML = template({name:data.name, manager:data.manager});
            var page = {
                    title : "Welcome pack - " + data.name,
                    space : { key : "ds"}, //~"+data.username},
                    body : {
                            storage : {
                                representation : "storage", 
                                value : storageXHTML 
                            } 
                    },
                    type : "page"
            };
            postNewPage(page);
    };

    function getConfluenceAPI(){
          
          var options = {
           uri: 'http://localhost:1990/confluence/rest/api/content/',
           headers: {
              'Authorization': 'Basic ' +
                new Buffer('admin' + ':' + 'admin').toString('base64') //credentials
             }
          };

          return options;
    };

    function postNewPage(page){
            var options = getConfluenceAPI();
            options.json = page;
            return request.post(options,
                    function (error, response) {
                        if (!response || response.statusCode != 200) {
                            if (response)
                                console.log(response.statusCode + " Error updating page : "+response.json);
                            else
                                console.log(error.message);
                        } else {
                            console.log('success');
                            return response.body;
                        }
              });
    }

    // Add any additional route handlers you need for views or REST resources here...


    // load any additional files you have in routes and apply those to the app
    {
        var fs = require('fs');
        var path = require('path');
        var files = fs.readdirSync("routes");
        var hbs = require('express-hbs');
        var request = require('request');
        for(var index in files) {
            var file = files[index];
            if (file === "index.js") continue;
            // skip non-javascript files
            if (path.extname(file) != "js") continue;

            var routes = require("./" + path.basename(file));

            if (typeof routes === "function") {
                routes(app, addon);
            }
        }
    }
};
