(function() {
  var getUrlParam = function (param) {
    var codedParam = (new RegExp(param + '=([^&]+)')).exec(window.location.search)[1];
    return decodeURIComponent(codedParam);
  };

  var baseUrl = getUrlParam('xdm_e') + getUrlParam('cp');
  $.getScript(baseUrl + '/atlassian-connect/all.js', function() {

		function onClickHandler(){
			console.log("on click");
			AP.require('dialog', function(dialog){
			 	  var d = dialog.create({
				  	header: 'Create welcome pack',
				    key: 'create-welcome-pack-dialog',
				    width: '500',
				    height: '250',
				    chrome: true
				  }).on('close', postWelcomePack);
			});

		};

		$("#welcomepack").click(onClickHandler);
	});
})();