// Mock of our remote services
module.exports = {
  findJIRAOwner: function (jiraIssue) {
    // mock out owner
    return "admin"
  },
  calcMetric: function (metricName) {
    // mockout the metric with random numbers
    return (Math.random() * 100).toFixed(1);
  }
};
