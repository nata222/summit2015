Tailoring Confluence for Team Productivity
==========================================

This repository contains resources and code examples for the 2015 Atlassian Summit Talk, Tailoring Confluence for Team Productivity, by Ben Mackie and Steve Lancashire.  There are three examples in this repository:

* Creating a report of LDAP users in a Confluence Page
* A blue print for an Operations report
* A connect add-on to create HR welcome pack for a new starter

How do I get set up?
-----------------------

### Prerequisites ###
Install the [atlassian SDK](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project)

Install [node.js](https://nodejs.org/en/)

### Getting started ###
Launch a local version of confluence with connect enabled as per the [tutorial](https://developer.atlassian.com/static/connect/docs/latest/guides/confluence-gardener-tutorial.html), i.e.: 

    atlas-run-standalone --product confluence --version 5.9.1-OD-2015.42.1-0002 --data-version 5.9.1-OD-2015.42.1-0002 --bundled-plugins com.atlassian.bundles:json-schema-validator-atlassian-bundle:1.0.4,com.atlassian.webhooks:atlassian-webhooks-plugin:1.0.6,com.atlassian.jwt:jwt-plugin:1.2.2,com.atlassian.upm:atlassian-universal-plugin-manager-plugin:2.20.1-D20150924T170115,com.atlassian.plugins:atlassian-connect-plugin:1.1.54 --jvmargs -Datlassian.upm.on.demand=true

Example 1: A script to automate publishing of LDAP membership into confluence
------------------------------------------------------------------------------
This example queries an LDAP server extracting group membership information and
publishing that information on a page in confluence using the confluence REST API. The example is in the ldap-page directory.

###Starting the LDAP server###
We'll start an local LDAP server for our testing

    node server.js

Our script will be updating an existing page, so create a new page in confluence, and get it's ID.  The easiest way to
grab the page id is to navigate the pages history and grab the pageId from the URL.  We'll pass this to our script like so:

    node ldap-script.js 1234

 This updates the page with ID 1234, displaying our group membership in a table.  You can now visit the updated page in Confluence.



Example 2: A connect blueprint that publishes an Operations report into Confluence
---------------------------------------------------------------------------------
   
The second example is in the blueprint directory.  It is a connect blueprint that renders
a handle bars template to generate an Operations report pulling together disparate datasources.

Launch the connect add-on:

    cd blueprints
    npm start

This will install the add-on into the confluence instance running locally on 1990.

The blueprint template route is defined in routes/index.js and this renders the template in views/operational-review.xml.hbs.  The template is populated with data provided from remoteData.js.

You should be able to create the operational report from the confluence create dialog now.

Example 3: A connect add-on to create a welcome pack for new starters in Confluence
-----------------------------------------------------------------------------------

The third example is in the welcomepack directory.  It is a connect add that provides
a number of connect modules that allow us to create a personalised welcome pack for
new starts at a company.  It comprises a connect macro and a dialog that posts some 
form data back to the add-on, which then uses the Confluence REST API and elements of Confluence 
Storage format to create a number of pages in Confluence.

Launch the connect add-on: 

    cd welcomepack
    npm start

This installs the add-on into the confluecne instance running locally on 1990.

The create welcome pack button is defined as a macro in atlassian-connect.json, this registers
a macro called "Welcome Pack" in Confluence.  Add this macro to a page in confluence.  Once
the page is saved this macro appears as a button with text "Create Welcome Pack".  Clicking
this button shows our form dialog, this is created dynamically by the call to dialog.create in 
client-create-wp.js. The dialog uses the page module "create-welcome-pack-dialog" for its content, which is also
listed in atlassian-connect.json. The dialog uses client-dialog.js to control its behaviour..., 
