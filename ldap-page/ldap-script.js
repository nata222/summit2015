
var assert = require('assert');
var hbs = require('handlebars');
var fs = require('fs');
var request = require('request');
var ldap = require('ldapjs');


searchLdap(updateConfluence);


function searchLdap(publishToConfluenceCallback){
  var ldapClient = ldap.createClient({
    url: 'ldap://127.0.0.1:1389'
  });

  ldapClient.bind('cn=root', 'secret', function(err) {
    assert.ifError(err);
  });

  var groups = {};
  var users = {};

  // part 1 - find all users
  ldapClient.search('o=empire', {filter:'(objectClass=*)'}, function(err,res) {
    res.on('searchEntry', function(entry) {
      var o = entry.object;
      switch(o.objectClass) {
        case 'inetOrgPerson': users[entry.dn] = o; break;
        case 'groupOfNames': groups[entry.dn] = o; break;
        default: console.log(JSON.stringify(o)); break;
      }
    });

    res.on('end', function() {
        // part 2 - manipulate the data
        var result = [];
        for (var key in groups) {
          var g = groups[key];
          var group = { name: g.cn, members: [] };
          for (var i = 0; i < g.member.length; i++) {
            var u = users[g.member[i]];
            group.members.push({name:u.uid});
          }
          result.push(group);

        }
        publishToConfluenceCallback(result);

        ldapClient.unbind(function(err) {
          assert.ifError(err);
        });
    });
  });
}

function updateConfluence(ldapData){
  var pageId = process.argv[2];
  if (!pageId)
    console.error("Please provide the page id to update as the first argument to the script");

  var options = {
   uri: 'http://localhost:1990/confluence/rest/api/content/'+pageId+"?expand=body.storage,version", // hard code the page to update
   headers: {
      'Authorization': 'Basic ' + 
        new Buffer('admin' + ':' + 'admin').toString('base64') //credentials
     }   
  };
  
  var template = hbs.compile(fs.readFileSync('storage.hbs','utf8'));
  var html = template({groups:ldapData});    
 
  request.get(options, 
    function (error, response, pageData) {
      if (response && response.statusCode == 200) {
        pageData = JSON.parse(pageData);
        pageData.body.storage.value = html;
        pageData.version.number = pageData.version.number + 1;
        options.body = pageData;
        options.json = true;
        request.put(options, function (error, response) {
            if (response.statusCode != 200) {
              console.log(response.statusCode + 
                " Error updating page : "+response.body);
            } else {
              console.log("Successfully updated page : "+pageData._links.webui);
            }
          });

      } else { 
        console.log("Error could not update the page with id : " + pageId + ", error code:" +response.statusCode);
      }
    });
}














