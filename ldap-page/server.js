var ldap = require('ldapjs');

var org = 'empire';
var server = ldap.createServer();

function authorize(req, res, next) {
  if (!req.connection.ldap.bindDN.equals('cn=root'))
    return next(new ldap.InsufficientAccessRightsError());
  return next();
}

function createGroup(name, members) {
  return  { 
    dn: 'cn='+name+', ou=groups, o='+org,
    attributes: {
      cn: name,
      objectClass: 'groupOfNames',
      member: members
    }
  };
}

function createUser(o) {
  return  { 
    dn: 'cn='+o.cn+', ou=users, o='+org,
    attributes: {
      cn: o.cn,
      sn: o.sn,
      uid: o.cn,
      memberOf: o.memberOf,
      objectClass: 'inetOrgPerson'
    }
  };
}

function loadUsers(req, res, next) {
    req.users = {};
    req.users['dvader'] = createUser({cn:'dvader',sn:'vader',memberOf:['leaders']});
    req.users['spalpatine'] = createUser({cn:'spalpatine',sn:'palpatine',memberOf:['leaders']});

    req.groups = {};
    req.users['leaders'] = createGroup('leaders', [req.users['dvader'].dn,req.users['spalpatine'].dn]);
    return next();
}

server.bind('cn=root', function(req, res, next) {
  if (req.dn.toString() !== 'cn=root' || req.credentials !== 'secret')
    return next(new ldap.InvalidCredentialsError());

  res.end();
  return next();
});

pre = [authorize,loadUsers];

server.search('o='+org, pre, function(req, res, next) {
  Object.keys(req.users).forEach(function(k) {
    if (req.filter.matches(req.users[k].attributes))
      res.send(req.users[k]);
  });

  res.end();
  return next();
});

server.listen(1389, function() {
  console.log('/etc/passwd LDAP server up at: %s', server.url);
});
