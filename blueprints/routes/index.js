module.exports = function (app, addon) {

    var moment = require('moment');
    var remoteData = require('../remoteData');

    app.get('/', function (req, res) {
        res.format({
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });

    app.get('/operational-review.xml',
        function (req, res) {
            nocache(res);

            var reliability = {};
            reliability.owner = remoteData.findJIRAOwner("CONF-16223");
            reliability.metric = remoteData.calcMetric("reliability");

            var performance = {};
            performance.owner = remoteData.findJIRAOwner("CONF-223");
            performance.metric = remoteData.calcMetric("performance");
            var currentDate = moment().format("YYYY-MM-DD");

            res.render('operational-review.xml.hbs', {
                "reliability": reliability,
                "performance": performance,
                "date": currentDate
            });
        }
    );

    function nocache(res){
        res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
        res.header('Expires', '-1');
        res.header('Pragma', 'no-cache');
    }
};
