(function() {

	  function postWelcomePack(formData, jwtToken, dialog) {
			console.log("postWelcomePack");

			$.ajax({
		        url: '/welcome-pack',
		        type: 'POST',
		        dataType: 'json',
		        data: formData,
		        beforeSend: function (request) {
				    request.setRequestHeader("Authorization", "JWT "+jwtToken);
				},
		  	    success: function(responseText){
			      AP.require("messages", function(messages){
					  var message = messages.info('Welcome pack created successfully',"");
					  dialog.close();
					})
			      	
			    },
			    error: function(response){
			    	AP.require("messages", function(messages){
					  var message = messages.error('Couldn\'t create welcome pack', 
					  	'Error: '+response.responseText);
					  dialog.close();
					});
					
			    }
			  });
		};

	  var getUrlParam = function (param) {
	    var codedParam = (new RegExp(param + '=([^&]+)')).exec(window.location.search)[1];
	    return decodeURIComponent(codedParam);
	  };

	  var baseUrl = getUrlParam('xdm_e') + getUrlParam('cp');

	  $.getScript(baseUrl + '/atlassian-connect/all.js', function() {
		   AP.require(["dialog"], function(dialog) {
		    dialog.getButton('submit').bind(function() {
		        postWelcomePack($('form#welcome-pack-form').serialize(), $("meta[name='jwtToken']").attr('content'), dialog);
		    });
		});
	});
})();
